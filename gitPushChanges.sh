#!/bin/bash
gitSetIdentity () {
	git config --global user.email "seth@migo.tv"
	git config --global user.name "Seth Palileo"
}
gitPushChanges () {
	git init
	git add .
	git commit -m "bamboo changes"
	git remote add repo https://SethPalileo@bitbucket.org/SethPalileo/devopsinfra.git
	git push
}
gitSetIdentity >> gitfile.log
gitPushChanges >> gitfile.log
exit
