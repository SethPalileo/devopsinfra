#!/bin/bash
source ./hosts.txt
for RAMLITE in "${HOSTS[@]}";
do
	:
	echo "Connecting to ramlite: $RAMLITE"
	if [ $RAMLITE == $1 ]; then
		echo "This ramlite@$RAMLITE is the source"
		continue
	fi
	echo "ramlite@$RAMLITE is updating"
	putty ramlite@$RAMLITE -pw IM@gin33r -m "C:\cygwin64\devops\devopsinfra\rak.sh"
done